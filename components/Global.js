import { createGlobalStyle } from 'styled-components';
import { normalize } from 'polished';

const Global = createGlobalStyle`
  ${normalize()};
  html {
    box-sizing: border-box;
    color: #fefefe;
    font-family: 'Raleway', sans-serif;
  }
  *, *::before, *::after {
    box-sizing: inherit;
  }
  body {
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    width: 100vw;
    height: 100vh;
    background: #48484F;
  }
  main {
    width: 100vw;
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  h1, h2, h3, h4, h5, h6 {
    font-weight: 900;
  }
  footer {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    text-align: center;
    padding-bottom: 2em;
  }
`;

export default Global;
