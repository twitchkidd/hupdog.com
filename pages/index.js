import Head from 'next/head';
import Image from 'next/image';

export default function Index() {
	return (
		<>
			<Head>
				<title>HupDog PT</title>
				<meta
					name='description'
					content='Developed with NextJS and heart emoji'
				/>
				<link rel='icon' href='/favicon.ico' />
			</Head>
			<main>
				<Image
					src='/images/logo.png'
					height={120}
					width={120}
					alt='Hup Dog logo, cartoon dog with red headband'
				/>
				<h1>HupDog Personal Training</h1>
				<p>Website currently under construction!</p>
			</main>
			<footer>Made with NextJS and ❤️ | ©2021 Gareth Field</footer>
		</>
	);
}
